### `Use npm start to start server, before it you will have to install all packages`

`Your tasks:`

- Display client table.
- Create login/logout form.
- Add possibility to edit client `*`
- Add possibility to delete client`*`
- Add possibility to add new client `*`
- Add possibility to open client and display his info on another page`*`

`Note`

- Unauthorized user can see only login page and table with clients, he doesnt have possibility to manipulate clients and open them on another page . All pages which can change clients database have to be hidden from this user.
- Authorized user can see table with clients, also he is granted access to open client on another page, edit, delete and add user.

`Requirements:`

- React js
- React router
- Your application should consist of at least 4 routes : "/", "/login", "/user" , "/create-user". You can name urls as you wish =)

`*` - means, that api is protected and it requires special `authorization` header in request.
To get `authorization` token you need to login , use api `/user/login`   `login`: "Admin", `password`: "1234"

`Endpoints : `

- POST http://localhost:3333/clients/add `params: name, surname, age, phone`
- POST http://localhost:3333/clients/get `params: id`
- DELETE http://localhost:3333/clients/remove?id=id `params: id`
- PUT http://localhost:3333/clients/edit   `params: name, surname, age, phone, id`
- GET http://localhost:3333/clients

- POST http://localhost:3333/user/login `params: login, password`





